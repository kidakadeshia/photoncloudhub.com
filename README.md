## Git + Github Setup

Lege Dir bitte einen eigenen GitHub Account an, https://github.com/signup/free.

Installiere einen Client für Git ...

Windows:
* [http://windows.github.com/](http://windows.github.com/)
* ... ODER ...
* [http://code.google.com/p/msysgit/downloads/list](http://code.google.com/p/msysgit/downloads/list)
* UND(!)
* [http://code.google.com/p/tortoisegit/downloads/list](http://code.google.com/p/tortoisegit/downloads/list) 

MAC:
* [http://sourcetreeapp.com/download/](http://sourcetreeapp.com/download/)

Richte Git ein ...
* [https://help.github.com/articles/set-up-git](https://help.github.com/articles/set-up-git)
* [https://help.github.com/articles/generating-ssh-keys](https://help.github.com/articles/generating-ssh-keys)

... und ziehe Dir dann eine Kopie.


## Markdown

Die Artikel sind in sogenannter **Markdown** verfasst.
Einfacher Fliesstext wird dazu zur Anzeige in HTML geparsed.
Die Syntax ist schnell gelernt:
[http://daringfireball.net/projects/markdown/syntax](http://daringfireball.net/projects/markdown/syntax)


## Artikel Verfassen

Jeder Artikel beginnt mit einem Header (aka YAML Frontmatter),
[https://github.com/mojombo/jekyll/wiki/yaml-front-matter](https://github.com/mojombo/jekyll/wiki/yaml-front-matter):

<pre class="code">
<code class="brush: xml; first-line: 1;">
---
layout: article
title: Titel der Seite
categories: [Name_der_Kategorie, ...]
tags: [setup, installation, how-to, quickstart, overview, operations, demo, sample, lite, lobby, unity, mmo]
---
{% include globals %}
</code>
</pre>

Das s.g. YAML Frontmatter ist hier beispielhaft aufgeführt.
Jeder Artikel muss(!) mit den o.g. 7 Zeilen beginnen:
* Layout: Hier muss immmer "article" stehen (ohne "")
* Title: Bestimmt den Titel der Seite, die Überschrift des Artikels und den Namen des Posts innerhalb der Navigation.
  Dieser sollte eindeutig sein und darf kein " : " (Doppelpunkt) Zeichen beinhalten! 
* Categories: Bestimmt in welcher Navigation der Post abgelegt wird.
  Es muss mindestens [photon-cloud] und/oder [photon-server] angegeben werden, sowie nur EINER der drei Navigationsbereiche [getting_started, references, tutorials]. 
* Tags: Sollten angemessen verwendet werden.

Die eigentliche .md Datei wird wie folgt benannt:

XXXX-YY-ZZ-Name.md

* XXXX = Ordnungsnummer, die bestimmt an welcher Stelle in der Liste der Navigation der Eintrag steht.
  Beginnend bei 1900 wird in 10er Schritten iteriert.
  Falls später weitere Seiten eingefügt werden müssen, kann man so relativ genau bestimmen wo der Eintrag in der Liste angezeigt wird.
* YY = Eigentlich egal, ich nehme immer 11
* ZZ = Eigentlich egal, ich nehme immer 11
* Name = Title des Posts, Leerzeichen werden mit Unterstrichen dargestellt.

Beispiel:
1910-11-11-starting_photon_in_5_minutes.md
Hier wurde 1920 gewählt, damit es an zweiter Stelle in der entsprechenden Navigation angezeigt wird.
Zum genauen bestimmen der Ordnungsnummer muss vorher ein Blick auf die Liste der Posts geworfen werden.
Diese kann man leicht online einsehen: Einfach die Stelle aussuchen, an der der Post erscheinen soll.
Dann mit der Maus über den Vorhergehenden und Nachfolgenden Post hovern, im Link ist dann nun die Ordnungsnummer einsichtbar!

## Beispiele

### Überschriften

Größe der Überschriften wird bestimmt durch die Anzahl der Vorangestellten '#':
<pre class="code">
<code class="brush: xml; first-line: 1;">
## Dies ist eine H2 Überschrift
### Dies ist eine H3 Überschrift
</figure>
</code>
</pre>

Achtung: Keine H1 überschriften verwenden, diese werden automatisch durch den "title" (YAML Frontmatter) gesetzt. Im Fliesstext des Artikels verwende dann nur noch H2+.


### Links

Links werden im Format
<pre class="code">
<code class="brush: xml; first-line: 1;">
[Text des Links] (URL)
</code>
</pre>
beschrieben.
Wenn ein Post direkt verlinkt werden soll:
<pre class="code">
<code class="brush: xml; first-line: 1;">
[Link zum Post Photon in 5 Minutes] (/PhotonIn5Minutes)
</code>
</pre>

Um mit Hilfe einer ID einen Link direkt an eine gewünschte Stelle im Dokument springen zu lassen setzt man zuerst seine ID an die Stelle an die gesprungen werden
soll:

<pre class="code">
<code class="brush: xml; first-line: 1;">
&lt;i id=&quot;hierSpringHin&quot;&gt;&amp;nbsp;&lt;/i&gt;
</code>
</pre>

nun wird der der Link wie folgt aufgerufen:

<pre class="code">
<code class="brush: xml; first-line: 1;">
[Link zur ID](#hierSpringHin)
</code>
</pre>


### Bilder

Bilder werden eingebunden indem sie in den Ordner 'assets/img' abgelegt und dann in dem Post wie folgt referenziert werden:

<pre class="code">
<code class="brush: xml; first-line: 1;">
&lt;figure&gt;
&lt;img src=&quot;{{ IMG }}/preview_download_photon_unity_viking-demo.png&quot; /&gt;
&lt;figcaption&gt;Bildunterschrift&lt;/figcaption&gt;
&lt;/figure&gt;
</code>
</pre>

Die HTML-Tags &lt;figure&gt; und &lt;figcaption&gt; sind optional.
Ohne beide wird das Bild im Fliesstext zur Anzeige gebracht.


### Code Samples

Um Code samples anzuzeigen wird die folgende Syntax verwendet:

<pre class="code">
<code class="brush: xml; first-line: 1; highlight: [5,6]">
&lt;pre class="code"&gt;
&lt;code class="brush: xml; first-line: 1; highlight: [5,6]"&gt;
   if (crouch)
   {
      if (aim)
      { 
       animation["CrouchWalkAim"].speed = 1.0F;
       animation.CrossFade("CrouchWalkAim");
      }
      else
      {
         animation["CrouchWalk"].speed = 1.0F;
         animation.CrossFade("CrouchWalk");
      }
   }
&lt;/code&gt;
&lt;/pre&gt;
</code>
</pre>

In der zweiten Zeile bestimmt der Parameter "first-line: 1" die Zeilennummer, mit der der Codeblock beginnt.
"highlight: [5,6]" besagt das die 5te und 6te Zeile farblich hervorgehoben werden.


### Slideshare

Suche die gewünschte Präsentation auf [Slideshare.net](http://www.slideshare.net).
Auf der Präsentationsseite klicke den Link "<> Embed", z.Z. oberhalb der jeweiligen Präsentation.
Kopiere das einzubettende HTML in den Artikel, ergänze im iframe Tag das Attribut 'allowfullscreen' mit '="true"'... speichern und voila!

Beispielquellcode:

<pre class="code>
<code class="brush: xml; first-line: 1; highlight: [5]">
&lt;div style=&quot;width:425px&quot; id=&quot;__ss_13311008&quot;&gt;
  &lt;strong style=&quot;display:block;margin:12px 0 4px&quot;&gt;
    &lt;a href=&quot;http://www.slideshare.net/jess3/visual-storytelling-101&quot; title=&quot;Visual Storytelling 101&quot; target=&quot;_blank&quot;&gt;Visual Storytelling 101&lt;/a&gt;
  &lt;/strong&gt;
  &lt;iframe src=&quot;http://www.slideshare.net/slideshow/embed_code/13311008?rel=0&quot; width=&quot;425&quot; height=&quot;355&quot; frameborder=&quot;0&quot; marginwidth=&quot;0&quot; marginheight=&quot;0&quot; scrolling=&quot;no&quot; style=&quot;border:1px solid #CCC;border-width:1px 1px 0&quot; allowfullscreen=&quot;true&quot;&gt;&lt;/iframe&gt;
  &lt;div style=&quot;padding:5px 0 12px&quot;&gt;
    View more presentations from &lt;a href=&quot;http://www.slideshare.net/jess3&quot; target=&quot;_blank&quot;&gt;JESS3&lt;/a&gt;
  &lt;/div&gt;
&lt;/div&gt;
</code>
</pre>